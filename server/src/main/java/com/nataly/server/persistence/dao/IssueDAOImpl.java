package com.nataly.server.persistence.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nataly.server.persistence.model.Issue;

@Repository
public class IssueDAOImpl implements IssueDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void persistIssue(Issue issue) {
		sessionFactory.getCurrentSession().persist(issue);
	}
	
}
