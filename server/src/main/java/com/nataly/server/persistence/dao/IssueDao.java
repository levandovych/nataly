package com.nataly.server.persistence.dao;

import com.nataly.server.persistence.model.Issue;

public interface IssueDao {
	  void persistIssue(Issue issue);
}
