package com.nataly.server.service;

import com.nataly.server.persistence.model.Issue;

public interface IssueService {
	void persistIssue(Issue issue);
}
