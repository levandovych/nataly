package com.nataly.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nataly.server.persistence.dao.IssueDao;
import com.nataly.server.persistence.model.Issue;

@Service
public class IssueServiceImpl implements IssueService {

	@Autowired
	private IssueDao issueDAO;

	@Transactional
	public void persistIssue(Issue issue) {
		issueDAO.persistIssue(issue);
	}

}
