package com.nataly.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nataly.server.persistence.model.Issue;
import com.nataly.server.service.IssueService;
   
@Controller    
public class MainController {
	
	@Autowired
	private IssueService issueService;
   
    @RequestMapping(value = "/", method = RequestMethod.GET)  
	public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello! This is Nataly Issue Tracking System.");  
        return "index";  
    }  
}
